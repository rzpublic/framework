package text

import (
	. "github.com/smartystreets/goconvey/convey"
	"regexp"
	"testing"
)

func TestSlugify(t *testing.T) {

	const testString = " Это тестовая   заба--гованная строка со всякими там $pecsimVolam!  "
	slug := Slugify(testString)
	Convey("^- is absent", t, func() {
		So(slug, ShouldNotStartWith, "-")
	})

	Convey("-$ is absent", t, func() {
		So(slug, ShouldNotEndWith, "-")
	})

	Convey("Space is absent", t, func() {
		So(slug, ShouldNotContainSubstring, " ")
	})

	Convey("'--' is absent", t, func() {
		So(slug, ShouldNotContainSubstring, "--")
	})

	Convey("upper case is absent", t, func() {
		result := regexp.MustCompile(`^[-а-яa-z]+$`).MatchString(slug)
		So(result, ShouldBeTrue)
	})
}

func TestUnslugufy(t *testing.T) {
	testTable := [...]string{
		"это-нормальная-тестовая-строка",
		"это-не очень нормальная-тестовая-строка",
		"--- всякие штуки в начале-и-конце  ",
		"БОЛЬШИЕ буквы",
	}

	for _, testCaseString := range testTable {
		Convey(testCaseString, t, func() {
			testCaseUnslugify(testCaseString)
		})

	}
}

func testCaseUnslugify(slug string) {
	text := Unslugify(slug)
	Convey("space before is absent", func() {
		So(text, ShouldNotStartWith, " ")
	})

	Convey("space after is absent", func() {
		So(text, ShouldNotEndWith, " ")
	})

	Convey("contains only acceptable chars", func() {
		result := regexp.MustCompile(`^(\s|[а-яА-Яa-zA-Z])+$`).MatchString(text)
		So(result, ShouldBeTrue)
	})

	Convey("2 space is absent", func() {
		So(text, ShouldNotContainSubstring, "  ")
	})
}

func TestMakePreview(t *testing.T) {
	testTable := map[string]int{
		"ы":        1,
		"text....": 5,
		"t.....":   4,
		"sdfasdf?": 3,
		"Рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. ": 60,
	}

	for testCaseText, length := range testTable {
		Convey(testCaseText, t, func() {
			testCaseMakePreview(testCaseText, length, PreviewDefaultEndsWith)
		})
	}
}

func testCaseMakePreview(text string, maxLength int, endsWith string) {
	preview := MakePreview(text, maxLength, endsWith)
	textLen := len([]rune(text))
	previewLen := len([]rune(preview))

	Convey("check maxLength", func() {
		So(previewLen, ShouldBeLessThanOrEqualTo, maxLength)
	})

	if textLen > maxLength {
		Convey("ends with "+PreviewDefaultEndsWith, func() {
			So(preview, ShouldEndWith, PreviewDefaultEndsWith)
		})
	}
}


func TestTranslit(t *testing.T) {
	testTable := map[string]string{
		`Привет`:`Privet`,
		`Как дела`:`Kak dela`,
		`Символы`:`Simvoly`,
	}

	Convey("TestTranslit", t, func() {
		for rusText, enText := range testTable {
			Convey(rusText + " to " + enText, func() {
				So(TranslitRuToLatin(rusText), ShouldEqual, enText)
			})
		}
	})

}