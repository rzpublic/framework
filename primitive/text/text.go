package text

import (
	"gitlab.com/rzpublic/translit"
	"regexp"
	"strings"
)

// PreviewDefaultEndsWith - symbols that shows after text preview. You can use this by default
const PreviewDefaultEndsWith = "..."

var makePreviewNextSymbolRegexp,
	makePreviewWordsEndRegexp,
	doubledWhiteSpaces *regexp.Regexp

func init() {
	makePreviewNextSymbolRegexp = regexp.MustCompile("(?i)[^а-яa-z]") // TODO fix to all languages
	makePreviewWordsEndRegexp = regexp.MustCompile("(?i)[^а-яa-z](?:[а-яa-z]*$)")
	doubledWhiteSpaces = regexp.MustCompile(`\s{2,}`)
}

// MakePreview - text preview making
func MakePreview(text string, maxLength int, endsWith string) (returnText string) {
	returnText = text
	isStripped := false
	runes := []rune(text)
	runesLen := len(runes)
	endsWithLength := len([]rune(endsWith))
	splitPosition := maxLength - endsWithLength

	defer func() {
		if isStripped {
			returnText += endsWith
		}
	}()

	if runesLen <= maxLength {
		return
	}

	returnText = string(runes[0:splitPosition])
	isStripped = true

	// if next symbol after split is not alphabetic return as is
	if nextSymbol := string(runes[splitPosition]); makePreviewNextSymbolRegexp.MatchString(nextSymbol) {
		return
	}

	// search of next split index
	submatchIndexes := makePreviewWordsEndRegexp.FindStringSubmatchIndex(returnText)
	if submatchIndexes == nil {
		return
	}

	wordsEndPosition := submatchIndexes[0]
	returnText = returnText[0:wordsEndPosition]

	return
}

// Slugify - do human and seo readable text for url
func Slugify(phrase string) string {
	slug := strings.ToLower(phrase)
	slug = regexp.MustCompile(`[^-a-zа-я]`).ReplaceAllString(slug, `-`)
	slug = regexp.MustCompile(`-{2,}`).ReplaceAllString(slug, `-`)
	slug = strings.Trim(slug, "-")

	return slug
}

// Unslugify - making the normal text from url slug
func Unslugify(slug string) string {
	text := strings.Replace(slug, "-", " ", -1)
	text = doubledWhiteSpaces.ReplaceAllString(text, ` `)
	text = strings.TrimSpace(text)

	return text
}


func TranslitRuToLatin(text string) string {
	return translit.EncodeToICAO(text)
}
