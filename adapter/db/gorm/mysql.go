package gorm

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/rzpublic/framework/adapter/config"
)

const (
	gormMysqlConnectionPattern   = "%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local&collation=%s"
	gormMysqlDriver              = "mysql"
	gormMysqlTableOptionsPattern = "ENGINE=InnoDB CHARSET=%s auto_increment=1"
)

func NewGORMMysqlConnection(config config.Configurator) *gorm.DB {
	subConfigObj := config.Sub("db")
	connectionString := gormMysqlBuildConnectionString(
		subConfigObj.GetString("host"),
		subConfigObj.GetString("port"),
		subConfigObj.GetString("user"),
		subConfigObj.GetString("password"),
		subConfigObj.GetString("database"),
		subConfigObj.GetString("charset"),
		subConfigObj.GetString("collation"),
	)

	tableOptions := gormMysqlBuildTableOptions(subConfigObj.GetString("charset"))

	connection := gormMysqlConnect(
		gorm.Open,
		connectionString,
		tableOptions,
		subConfigObj.GetInt("max_idle_conns"),
		subConfigObj.GetInt("max_open_conns"),
	)

	return connection
}


func gormMysqlConnect(
	connect gormConnectionFunc,
	connectionString, tableOptions string,
	maxIdleConnections, maxOpenConnections int,
) (*gorm.DB) {
	var db *gorm.DB
	var err error

	db, err = connect(gormMysqlDriver, connectionString)
	if err != nil {
		panic(err)
	}

	db.Set("gorm:table_options", tableOptions)
	db.DB().SetMaxIdleConns(maxIdleConnections)
	db.DB().SetMaxOpenConns(maxOpenConnections)

	return db
}


func gormMysqlBuildConnectionString(host, port, user, password, database, charset, collation string) string {
	return fmt.Sprintf(
		gormMysqlConnectionPattern,
		user,
		password,
		host,
		port,
		database,
		charset,
		collation,
	)
}

func gormMysqlBuildTableOptions(charset string) string {
	return fmt.Sprintf(gormMysqlTableOptionsPattern, charset)
}
