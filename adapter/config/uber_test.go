package config

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

var tableUberConfigTestCases = map[string]string{
	"webserver.host": "127.0.0.1",
	"webserver.port": "3001",
	"sphinx.index":   "idx_index",
	"db.host":        "192.168.0.1",
	"db.port":        "3306",
	"db.driver":      "mysql",
	"db.user":        "user",
	"db.password":    "password",
	"sub.sub.key":    "value",
}

func TestNewUberConfig(t *testing.T) {
	Convey("type of assert", t, func() {
		So(func() { NewUberConfig("non_exists_file") }, ShouldPanic)
	})
}

func TestUberConfigProvider_GetInt(t *testing.T) {
	configObj := NewUberConfig(testConfigYamlFileName)
	Convey("get int", t, func() {
		So(configObj.GetInt("webserver.port"), ShouldEqual, 3001)
		So(configObj.GetInt("db.port"), ShouldEqual, 3306)
		So(func() { configObj.GetInt("sphinx.index") }, ShouldPanic)
	})
}

func TestUberConfigProvider_GetString(t *testing.T) {
	configObj := NewUberConfig(testConfigYamlFileName)
	Convey("get string", t, func() {
		for key, expectedValue := range tableUberConfigTestCases {
			So(configObj.GetString(key), ShouldEqual, expectedValue)
		}
	})
}

func TestUberConfigProvider_IsSet(t *testing.T) {
	configObj := NewUberConfig(testConfigYamlFileName)
	Convey("is set", t, func() {
		for key := range tableUberConfigTestCases {
			So(configObj.IsSet(key), ShouldEqual, true)
		}
	})
}

func TestUberConfigProvider_Sub(t *testing.T) {
	configObj := NewUberConfig(testConfigYamlFileName)
	testExistTable := map[string][]string{
		"webserver": {"host", "port"},
		"db":    {"host", "port"},
	}

	Convey("sub", t, func() {
		var subConfig ProviderInterface
		for subKey, values := range testExistTable {
			subConfig = configObj.Sub(subKey)
			for _, key := range values {
				So(subConfig.IsSet(key), ShouldBeTrue)
			}
		}

		subConf := configObj.Sub("sub").Sub("sub")
		So(subConf.GetString("key"), ShouldEqual, "value")
	})
}
