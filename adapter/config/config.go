// Package config use file for read application configuration
package config

// ProviderInterface interface
type ProviderInterface interface {
	GetString(key string) string
	GetInt(key string) int
	Sub(key string) ProviderInterface
	IsSet(key string) bool
}

// Configurator - all realisations should implement it. for ex. see Config
type Configurator interface {
	ProviderInterface
}

// Config - proxy object
type Config struct {
	provider ProviderInterface
	Configurator
}

func (config *Config) setProvider(provider ProviderInterface) Configurator {
	config.provider = provider

	return config
}

func (config *Config) getProvider() ProviderInterface {
	return config.provider
}

// GetString proxy method
func (config *Config) GetString(key string) string {
	return config.getProvider().GetString(key)
}

// GetInt proxy method
func (config *Config) GetInt(key string) int {
	return config.getProvider().GetInt(key)
}

// Sub proxy method
func (config *Config) Sub(key string) ProviderInterface {
	return config.getProvider().Sub(key)
}

// IsSet proxy method
func (config *Config) IsSet(key string) bool {
	return config.getProvider().IsSet(key)
}
