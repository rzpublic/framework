package web_server

import (
	"crypto/tls"
	"fmt"
	"gitlab.com/rzpublic/framework/adapter/config"
	"gitlab.com/rzpublic/framework/adapter/log"
	"golang.org/x/crypto/acme/autocert"
	"net/http"
	"os"
	"os/signal"
	"time"
)

const serverStartedMessage = "Web-server started on %s:%s"

type WebServer struct {
	config  config.Configurator
	handler *http.Handler
	log     log.Logger
}

type WebServerInterface interface {
	Run()
}

func NewServer(config config.Configurator, handler *http.Handler, logObj log.Logger) WebServerInterface {
	return &WebServer{
		config,
		handler,
		logObj,
	}
}

func (serverInstance *WebServer) Run() {
	var runners []func()
	var httpTlsServer *http.Server
	handler := *serverInstance.handler
	serverConfig := serverInstance.config.Sub("webserver")

	host := serverConfig.GetString("host")
	port := serverConfig.GetString("port")

	if serverConfig.IsSet("ssl") {
		sslConfig := serverConfig.Sub("ssl")
		sslPort := sslConfig.GetString("port")
		httpTlsServer, handler = buildTlsServerObj(
			host,
			sslPort,
			sslConfig.GetString("cache_dir"),
			serverConfig.GetString("fqdn"),
			sslConfig.GetString("email"),
			handler,
		)

		if httpTlsServer != nil {
			runners = append(runners, func() {
				serverInstance.log.Info(fmt.Sprintf(serverStartedMessage, host, sslPort))
				if err := httpTlsServer.ListenAndServeTLS("", ""); err != nil {
					serverInstance.log.Error(err.Error())
				}
			})
		}
	}

	httpServer := buildServerObj(host, port, handler)

	runners = append(runners, func() {
		serverInstance.log.Info(fmt.Sprintf(serverStartedMessage, host, port))
		if err := httpServer.ListenAndServe(); err != nil {
			serverInstance.log.Error(err.Error())
		}
	})

	startRunners(runners...)

	osChannel := make(chan os.Signal, 1)
	callback := func() {
		serverInstance.log.Info("shutting down")
		os.Exit(0)
	}

	bindOnShutdown(&osChannel, callback)
}

func buildTlsServerObj(host, sslPort, cacheDir, fqdn, sslEmail string, handler http.Handler) (*http.Server, http.Handler) {
	acme := autocert.Manager{
		Cache:      autocert.DirCache(cacheDir),
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(fqdn),
		Email:      sslEmail,
	}

	httpTlsServer := buildServerObj(
		host,
		sslPort,
		handler,
	)

	httpTlsServer.TLSConfig = &tls.Config{GetCertificate: acme.GetCertificate}
	handler = acme.HTTPHandler(handler)

	return httpTlsServer, handler
}

func buildServerObj(host, port string, handler http.Handler) *http.Server {
	return &http.Server{
		Addr:         fmt.Sprintf("%s:%s", host, port),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      handler,
	}
}

func bindOnShutdown(osChannel *chan os.Signal, handler func()) {
	signal.Notify(*osChannel, os.Interrupt)

	<-*osChannel

	handler()
}

func startRunners(runners ...func()) {
	for _, runner := range runners {
		go runner()
	}
}
