package log

import "testing"

func TestNewOpLog(t *testing.T) {
	logger := NewOpLog()
	logger.Log("test", Debug)
	logger.Log("test", Info)
	logger.Log("test", Notice)
	logger.Log("test", Warning)
	logger.Log("test", Error)
	logger.Log("test", Critical)
}
