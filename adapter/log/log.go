package log

const (
	Debug = "DEBUG"
	Info = "INFO"
	Notice = "NOTICE"
	Warning = "WARNING"
	Error = "ERROR"
	Critical = "CRITICAL"
)

type Provider interface {
	Log(message string, logLevel string)
}

type Logger interface {
	Provider
	AddProviders(createNewLogs ...NewLogProviderFunc)
	Debug(message string)
	Info(message string)
	Notice(message string)
	Warning(message string)
	Error(message string)
	Critical(message string)
}


type Log struct {
	providers []Provider
	Logger
}

type NewLogProviderFunc func () Provider

func NewLog() Logger {
	return new(Log)
}

func (proxy *Log) AddProviders(createNewLogs ...NewLogProviderFunc) {
	for _, createNewLog := range createNewLogs{
		proxy.providers = append(proxy.providers, createNewLog())
	}
}

func (proxy *Log) broadcastToAllProviders(message string, logLevel string) {
	for _, provider := range proxy.providers {
		provider.Log(message, logLevel)
	}
}

func (proxy *Log) Debug(message string) {
	proxy.broadcastToAllProviders(message, Debug)
}

func (proxy *Log) Info(message string) {
	proxy.broadcastToAllProviders(message, Info)
}

func (proxy *Log) Notice(message string) {
	proxy.broadcastToAllProviders(message, Notice)
}

func (proxy *Log) Warning(message string) {
	proxy.broadcastToAllProviders(message, Warning)
}

func (proxy *Log) Error(message string) {
	proxy.broadcastToAllProviders(message, Error)
}

func (proxy *Log) Critical(message string) {
	proxy.broadcastToAllProviders(message, Critical)
}

func (proxy *Log) Log(message string, logLevel string) {
	proxy.broadcastToAllProviders(message, logLevel)
}

