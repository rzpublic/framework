package template

import (
	"bytes"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestCorrectTemplate(t *testing.T) {
	var writer, writer2 bytes.Buffer
	var data = mockViewModel{
		prefix: "prefix",
		text:   "text",
	}

	tpl := NewMockTemplate(mockTplCorrectTestPrefix)
	tpl.MustCompile(mockTplCorrectTestAdditionalPart).Render(&writer, data)

	tpl2 := NewMockTemplate(mockTplCorrectTestPrefix)
	tpl2.MustRender(mockTplCorrectTestAdditionalPart, &writer2, data)

	Convey("Test normal behavior", t, func() {
		So(tpl.GetError(), ShouldBeNil)
		So(writer.String(), ShouldEqual, data.prefix+":"+data.text)
		So(writer.String(), ShouldEqual, writer2.String())
	})
}
func TestIncorrectTemplate(t *testing.T) {
	var writer, writer2 bytes.Buffer
	var data1 = mockViewModel{
		prefix: "",
		text:   "...",
	}

	var data2 = mockViewModel{
		prefix: "...",
		text:   "",
	}

	Convey("Test incorrect behavior", t, func() {
		tpl := NewMockTemplate(mockTplIncorrectTestPrefix)

		Convey("NewMockTemplate", func() {
			So(tpl.GetError(), ShouldNotBeNil)
		})

		tpl.resetError()
		tpl.MustCompile(mockTplIncorrectTestAdditionalPart)
		Convey("MustCompile", func() {
			So(tpl.GetError(), ShouldNotBeNil)
		})

		tpl.resetError()
		tpl.Render(&writer, data1)
		Convey("Render1", func() {
			So(tpl.GetError(), ShouldNotBeNil)
			So(writer.String(), ShouldEqual, "")
		})

		tpl.resetError()
		tpl.Render(&writer, data2)
		Convey("Render2", func() {
			So(tpl.GetError(), ShouldNotBeNil)
			So(writer.String(), ShouldEqual, "")
		})
	})

	Convey("Test full incorrect", t, func() {
		tpl := NewMockTemplate(mockTplIncorrectTestPrefix)
		tpl.MustCompile(mockTplIncorrectTestAdditionalPart).
			Render(&writer, data1)

		testFunc := func() {
			tpl2 := NewMockTemplate(mockTplIncorrectTestPrefix)
			tpl2.MustRender(mockTplIncorrectTestAdditionalPart, &writer2, data1)
		}
		So(testFunc, ShouldPanic)
	})
}
