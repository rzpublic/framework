package i18n

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSuffix(t *testing.T) {
	testTable := []struct {
		locale        string
		count         int64
		expectedValue string
	}{
		{
			locale:        RuLocale,
			count:         1,
			expectedValue: "_v1",
		}, {
			locale:        RuLocale,
			count:         11,
			expectedValue: "_v3",
		}, {
			locale:        RuLocale,
			count:         12,
			expectedValue: "_v3",
		}, {
			locale:        RuLocale,
			count:         120,
			expectedValue: "_v3",
		}, {
			locale:        RuLocale,
			count:         22,
			expectedValue: "_v2",
		}, {
			locale:        RuLocale,
			count:         4,
			expectedValue: "_v2",
		}, {
			locale:        RuLocale,
			count:         3,
			expectedValue: "_v2",
		}, {
			locale:        RuLocale,
			count:         111,
			expectedValue: "_v3",
		},{
			locale:        RuLocale,
			count:         0,
			expectedValue: "_v3",
		},{
			locale:        "",
			count:         0,
			expectedValue: "",
		},
	}

	for _, testCase := range testTable {
		f:= QuantitativeFunc(testCase.locale)
		if f == nil {
			continue
		}
		assert.Equal(t, f(testCase.count), testCase.expectedValue)
	}
}
