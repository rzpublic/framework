package i18n

const (
	RuLocale = "ru"

)

type Translator interface {
	Translate(key string, args ...string) string
	TranslateQuantitative(str string, count int64) string
}

func QuantitativeFunc(locale string) func(count int64) string {
	switch locale {
	case RuLocale:
		return quantitativeRu
	default:
		return nil
	}
}

func quantitativeRu(count int64) string {
	lastDigit := count % 10
	lastTwoDigits := count % 100
	switch {
	case lastTwoDigits > 10 && lastTwoDigits < 20:
		return "_v3"
	case lastDigit > 1 && lastDigit < 5: // 2,3,4
		return "_v2"
	case lastDigit == 1:
		return "_v1"
	default:
		return "_v3"
	}
}
