package i18n

import (
	gorI18n "github.com/qor/i18n"
	"github.com/qor/i18n/backends/yaml"
)

type GorProvider struct {
	instance *gorI18n.I18n
	locale string
	Translator
}

func NewGor(localesDir , locale string) Translator {
	backend := yaml.New(localesDir)

	instance := gorI18n.New(backend)

	return &GorProvider{
		instance: instance,
		locale: locale,
	}
}

func (provider *GorProvider) Translate(key string, args ...string) string {
	interfaceArgs := make([]interface{}, len(args))
	for k, v := range args {
		interfaceArgs[k] = v
	}

	htmlTpl := provider.instance.T(provider.locale, key, interfaceArgs...)
	return string(htmlTpl)
}


func (provider *GorProvider) TranslateQuantitative(str string, count int64) string {
	key := str + QuantitativeFunc(provider.locale)(count)

	return provider.Translate(key)
}