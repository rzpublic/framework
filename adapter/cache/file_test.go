package cache

import (
	. "github.com/smartystreets/goconvey/convey"
	"os"
	"testing"
)

const day = 86400

func TestNewFileCache(t *testing.T) {
	type testUnit struct {
		inDir, expectedDir string
	}

	var testTable = [...]testUnit{
		{
			inDir:       "cache",
			expectedDir: "cache/",
		},
		{
			inDir:       "",
			expectedDir: os.TempDir() + "cache/",
		},
	}

	Convey("NewFileCache dirs", t, func() {
		for _, unit := range testTable {
			NewFileCache(unit.inDir, day)

			Convey("Dir as expectedDir" + unit.inDir, func() {
				fileInfo, err := os.Stat(unit.expectedDir)
				So(err, ShouldBeNil)
				So(fileInfo.IsDir(), ShouldBeTrue)
			})
		}
	})
}

func TestFile_GetAndSet(t *testing.T) {
	type testUnit struct {
		inDir, expectedFile, key string
		value []byte

	}
	var testTable = [...]testUnit{
		{
			inDir:       "cache",
			expectedFile: "cache/",
			key: "f1f2f3f4",
			value: []byte("some value"),
		},
		{
			inDir:       "",
			expectedFile: os.TempDir() + "cache/",
			key: "f1f2f3f5",
			value: []byte("some value2"),
		},
	}

	Convey("TestFile_GetAndSet", t, func() {
		for _, unit := range testTable {
			cache, err := NewFileCache(unit.inDir, day)
			cache.Set(unit.key, unit.value)
			So(err, ShouldBeNil)

			valueOut, err := cache.Get(unit.key)
			So(err, ShouldBeNil)
			So(string(valueOut), ShouldEqual, string(unit.value))

			err = cache.Delete(unit.key)
			So(err, ShouldBeNil)

			valueOut, err = cache.Get(unit.key)
			So(valueOut, ShouldBeNil)
			So(err, ShouldBeNil)

			// test expired cache
			cache, err = NewFileCache(unit.inDir, -1)
			So(err, ShouldBeNil)
			cache.Set(unit.key, unit.value)
			valueOut, err = cache.Get(unit.key)
			So(valueOut, ShouldBeNil)
			So(err, ShouldBeNil)
			cache.Delete(unit.key)
		}

		cache, err := NewFileCache("", day)
		So(err, ShouldBeNil)
		value, err := cache.Get("wrong key")
		So(value, ShouldBeNil)
		So(err, ShouldBeNil)

		value, err = cache.Get("")
		So(value, ShouldBeNil)
		So(err.Error(), ShouldEqual, FileKeyMinLengthError)


		err = cache.Set("", []byte("value"))
		So(err.Error(), ShouldEqual, FileKeyMinLengthError)


	})
}

func TestBuildPath(t *testing.T) {
	type testUnit struct {
		key, expectedPath string
	}

	var testTable = [...]testUnit{
		{
			key: "f1f2asd",
			expectedPath : "cache/f1/f2/asd",
		},
		{
			key: "f2f1qwe",
			expectedPath:  "cache/f2/f1/qwe",
		},
		{
			key: "123",
		},
	}

	Convey("TestBuildPath", t, func() {
		for _, unit := range testTable {
			filePath, err := buildFilePath("cache/", unit.key)
			if len(unit.key) < FileKeyMinLength {
				So(err.Error(), ShouldEqual, FileKeyMinLengthError)
			} else {
				So(filePath, ShouldEqual, unit.expectedPath)
				So(err, ShouldBeNil)
			}
		}
	})
}