package cache

type Cache interface {
	Set(key string, value []byte) error
	// if value is not found it should to be nil and error should to be nil too
	Get(key string) (value []byte, err error)
	Delete(key string) (err error)
}
