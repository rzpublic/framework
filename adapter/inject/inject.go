package inject

type Injector interface {
	Map(interface{})
	Apply(interface{}) error
	MustApply(interface{})
	Invoke(interface{}) error
	MustInvoke(interface{})
	InvokeExcept(interface{}, ...interface{}) error
	MustInvokeExcept(interface{}, ...interface{})
	MustApplyAndMap(instances ...interface{})
}

type Provider interface {
	Map(interface{})
	Apply(interface{}) error
	Invoke(interface{}) error
	InvokeExcept(obj interface{}, excepts ...interface{}) error
}

type Inject struct {
	provider Provider
	Injector
}

func (injector *Inject) Map(obj interface{}) {
	injector.provider.Map(obj)
}

func (injector *Inject) Apply(obj interface{}) error {
	return injector.provider.Apply(obj)
}

func (injector *Inject) MustApply(obj interface{}) {
	if err := injector.Apply(obj); err != nil {
		panic(err)
	}
}

func (injector *Inject) Invoke(obj interface{}) error {
	return injector.provider.Invoke(obj)
}

func (injector *Inject) MustInvoke(obj interface{}) {
	if err := injector.Invoke(obj); err != nil {
		panic(err)
	}
}

func (injector *Inject) InvokeExcept(obj interface{}, excepts ...interface{}) error {
	return injector.provider.InvokeExcept(obj, excepts...)
}

func (injector *Inject) MustInvokeExcept(obj interface{}, excepts ...interface{}) {
	if err := injector.InvokeExcept(obj, excepts...); err != nil {
		panic(err)
	}
}

func (injector *Inject) MustApplyAndMap(instances ...interface{})  {
	for _, instance := range instances{
		injector.MustApply(instance)
		injector.Map(instance)
	}
}