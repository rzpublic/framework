package inject

import (
	"bytes"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)


func TestInject(t *testing.T) {
	injector := NewCodegangstaInjector()
	first := &MockFirst{
		Title: "Dependency",
	}

	second := &MockSecond{
		Title: "second",
	}

	mustApplyFunc := func() {
		injector.MustApply(second)
	}
	mustInvokeFunc := func() {
		injector.MustInvoke(func(elem *MockFirst) {})
	}

	Convey("test injectors", t, func() {
		So(mustInvokeFunc, ShouldPanic)
		So(mustApplyFunc, ShouldPanic)

		injector.Apply(first)
		injector.Map(first)
		injector.Apply(second)
		injector.Map(second)


		injector.Invoke(func(elem *MockFirst) {
			So(elem.Title, ShouldEqual, "Dependency")
		})
		injector.Invoke(func(elem *MockSecond) {
			So(elem.Title, ShouldEqual, "second")
			So(elem.Dependency.Title, ShouldEqual, "Dependency")
		})

		So(mustInvokeFunc, ShouldNotPanic)
		So(mustApplyFunc, ShouldNotPanic)

	})
}


func TestInject_InvokeExcept(t *testing.T) {
	testInvoker := func (buffer bytes.Buffer)  {
		buffer.Write([]byte(" world!"))
	}

	injector := NewCodegangstaInjector()
	Convey("test injectors", t, func() {
		So(func() {
			var buffer bytes.Buffer
			buffer.Write([]byte("hello"))
			for i := 0; i != 1000; i++ {
				injector.InvokeExcept(testInvoker, buffer)
			}
		}, ShouldNotPanic)
	})
}