package inject

import (
	"gitlab.com/rzpublic/framework/adapter/inject/codegangsta"
)

type CodegangstaInjector struct {
	instance codegangsta.Injector
	Provider
}

func NewCodegangstaInjector() Injector {
	return &Inject{
		provider: &CodegangstaInjector{
			instance: codegangsta.New(),
		},
	}
}

func (injector CodegangstaInjector) Map(obj interface{}) {
	injector.instance.Map(obj)
}

func (injector CodegangstaInjector) Apply(obj interface{}) error {
	return injector.instance.Apply(obj)
}

func (injector CodegangstaInjector) Invoke(obj interface{}) error {
	_, err := injector.instance.Invoke(obj)

	return err
}

func( injector CodegangstaInjector) InvokeExcept(obj interface{}, excepts ...interface{}) error {
	_, err := injector.instance.InvokeExcept(obj, excepts...)

	return err
}