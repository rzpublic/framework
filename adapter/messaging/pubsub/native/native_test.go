package native

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPubSub(t *testing.T) {
	const payload = "1"
	const topic = "topic"
	const nonRegisteredEventName = "mySecondEvent"
	event := NewEvent(topic, []byte(payload))
	nonRegisteredEvent := NewEvent(nonRegisteredEventName, []byte(payload))
	pubSubObj := NewPubSub()

	result := ""
	consumer := func(payload []byte) error {
		result += string(payload)
		return nil
	}

	pubSubObj.Subscribe(topic, consumer)

	err := pubSubObj.Fire(nonRegisteredEvent)
	assert.NotNil(t, err)

	err = pubSubObj.Fire(event)
	assert.Equal(t, payload, result)
	assert.Nil(t, err)

	err = pubSubObj.Fire(event)
	assert.Equal(t, payload + payload, result)
	assert.Nil(t, err)
}
