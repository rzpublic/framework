package pubsub

type Topic = string
type Consumer = func(payload []byte) error

type Instance interface {
	Subscribe(topic Topic, consumer Consumer) error
	Fire(event Event) error
}

type Event interface {
	Topic() Topic
	Payload() []byte
}
