package paginator

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

type testCase struct {
	name                 string
	pageNoIn             uint
	pageNoExpected       uint
	maxPageNoExpected    uint
	countPerPageIn       uint
	countPerPageExpected uint
	totalExpected        uint
	totalIn              uint
	offsetExpected       uint
	countAroundCurrent   int
	isShowMinMax         bool
	isShowPrevNext       bool
	isFirstExpected      bool
	isLastExpected       bool
}

type expectedCursorValue struct {
	Value    uint
	IsActive bool
}

func TestPaginator(t *testing.T) {
	for _, testCase := range testTable {
		testPaginatorCase(t, testCase)
	}
}

func testPaginatorCase(t *testing.T, testCase testCase) {
	Convey(testCase.name, t, func() {
		p, err := NewPaginator(
			testCase.pageNoIn,
			testCase.countPerPageIn,
			testCase.totalIn,
			testCase.countAroundCurrent,
			testCase.isShowPrevNext,
			testCase.isShowMinMax,
		)

		Convey("pageNo as expected", func() {
			switch {
			case testCase.pageNoIn < 1:
				So(err.Error(), ShouldEqual, ErrorMsgPageNoLT1)
			case testCase.pageNoIn > p.GetMaxPageNo():
				So(err.Error(), ShouldEqual, ErrorMsgPageNoGTMaxPageNo)
			default:
				So(p.GetPageNo(), ShouldEqual, testCase.pageNoExpected)
			}
		})

		if err == nil {
			Convey("offset as expected", func() {
				So(p.GetOffset(), ShouldEqual, testCase.offsetExpected)
			})
		}

		Convey("limit as expected", func() {
			So(p.GetLimit(), ShouldEqual, testCase.countPerPageIn)
		})

		Convey("maxPageNo as expected", func() {
			So(p.GetMaxPageNo(), ShouldEqual, testCase.maxPageNoExpected)
		})

		Convey("total as expected", func() {
			So(p.GetTotal(), ShouldEqual, testCase.totalExpected)
		})

		Convey("isFirst as expected", func() {
			So(p.IsFirst(), ShouldEqual, testCase.isFirstExpected)
		})

		Convey("ifLast as expected", func() {
			So(p.IsLast(), ShouldEqual, testCase.isLastExpected)
		})

		Convey("test cursors", func() {
			var cursor *Cursor
			cursor = p.Next() // first
			if cursor == nil {
				return
			}

			//So(cursor.IsPrevious && p.isShowPrevNext, ShouldBeTrue)
			//So(cursor.Value == p.pageNo - 1 || cursor.Value == minPageNo, ShouldBeTrue)

			for ; cursor != nil; cursor = p.Next() {
				switch {
				case cursor.IsFirst:
					So(cursor.Value, ShouldEqual, minPageNo)
				case cursor.IsLast:
					So(cursor.Value, ShouldEqual, testCase.maxPageNoExpected)
				case cursor.IsPrevious:
					So(cursor.Value == p.pageNo-1 || cursor.Value == minPageNo, ShouldBeTrue)
				case cursor.IsNext:
					So(cursor.Value == p.pageNo+1 || cursor.Value == p.GetMaxPageNo(), ShouldBeTrue)
				}
				//So(cursor.Value != 0 && !cursor.IsPrevious && !cursor.IsNext, ShouldEqual, cursor.IsShowValue)
				So(cursor.IsCurrent == true, ShouldEqual, cursor.Value == p.pageNo)

				fmt.Println(cursor.Value)
			}
		})
	})
}



//func TestCursor(t *testing.T) {
//	testTable := [...]struct {
//		name string
//		pageNoIn uint
//		countPerPageIn uint
//		totalIn uint
//		countAroundCurrent int
//		isShowMinMax bool
//		isShowPrevNext bool
//		expectedCursors []Cursor
//	}{
//		{
//			name: "normal data",
//			pageNoIn: minPageNo,
//			countPerPageIn: CountPerPageDefault,
//			totalIn: 1000,
//			countAroundCurrent: 1,
//			isShowPrevNext: true,
//			isShowMinMax: true,
//			expectedCursors: []Cursor{},
//		},
//		{
//			name: "normal data",
//			pageNoIn: 5,
//			countPerPageIn: CountPerPageDefault,
//			totalIn: 1000,
//			countAroundCurrent: 0,
//			isShowPrevNext: true,
//			isShowMinMax: true,
//		},
//	}
//
//
//
//}



var testTable = [...]testCase{
	{
		name:                 "pageNoIn: 0",
		pageNoIn:             0,
		pageNoExpected:       1,
		maxPageNoExpected:    10,
		countPerPageIn:       100,
		countPerPageExpected: 100,
		totalExpected:        1000,
		totalIn:              1000,
		offsetExpected:       0,
		countAroundCurrent:   1000,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      false,
		isLastExpected:       false,
	},
	{
		name:                 "Normal data",
		pageNoIn:             1,
		pageNoExpected:       1,
		maxPageNoExpected:    10,
		countPerPageIn:       100,
		countPerPageExpected: 100,
		totalIn:              1000,
		totalExpected:        1000,
		offsetExpected:       0,
		countAroundCurrent:   -1,
		isShowMinMax:         false,
		isShowPrevNext:       false,
		isFirstExpected:      true,
		isLastExpected:       false,
	},
	{
		name:                 "test offset",
		pageNoIn:             2,
		pageNoExpected:       2,
		maxPageNoExpected:    10,
		countPerPageIn:       10,
		countPerPageExpected: 10,
		totalIn:              100,
		totalExpected:        100,
		offsetExpected:       10,
		countAroundCurrent:   1,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      false,
		isLastExpected:       false,
	},
	{
		name:                 "test maxPageNo",
		pageNoIn:             1,
		pageNoExpected:       1,
		maxPageNoExpected:    11,
		countPerPageIn:       10,
		countPerPageExpected: 10,
		totalIn:              101,
		totalExpected:        101,
		offsetExpected:       0,
		countAroundCurrent:   -1,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      true,
		isLastExpected:       false,
	},
	{
		name:                 "test maxPageNo (2)",
		pageNoIn:             1011,
		pageNoExpected:       1,
		maxPageNoExpected:    11,
		countPerPageIn:       10,
		countPerPageExpected: 10,
		totalIn:              101,
		totalExpected:        101,
		offsetExpected:       0,
		countAroundCurrent:   1,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      false,
		isLastExpected:       true,
	},
	{
		name:                 "test paginator 1",
		pageNoIn:             1,
		pageNoExpected:       1,
		maxPageNoExpected:    266,
		countPerPageIn:       10,
		countPerPageExpected: 10,
		totalIn:              2651,
		totalExpected:        2651,
		offsetExpected:       0,
		countAroundCurrent:   -2,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      true,
		isLastExpected:       false,
	},
	{
		name:                 "test paginator 2",
		pageNoIn:             1,
		pageNoExpected:       1,
		maxPageNoExpected:    1,
		countPerPageIn:       10,
		countPerPageExpected: 10,
		totalIn:              10,
		totalExpected:        10,
		offsetExpected:       0,
		countAroundCurrent:   5,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      true,
		isLastExpected:       true,
	},
	{
		name:                 "test paginator 3",
		pageNoIn:             5,
		pageNoExpected:       5,
		maxPageNoExpected:    100,
		countPerPageIn:       10,
		countPerPageExpected: 10,
		totalIn:              1000,
		totalExpected:        1000,
		offsetExpected:       40,
		countAroundCurrent:   5,
		isShowMinMax:         true,
		isShowPrevNext:       true,
		isFirstExpected:      false,
		isLastExpected:       false,
	},
}
