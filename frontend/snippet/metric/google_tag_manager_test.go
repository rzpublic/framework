package metric

import "testing"
import . "github.com/smartystreets/goconvey/convey"

const gtmExpected = `<script async src="https://www.googletagmanager.com/gtag/js?id=123"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '123');
    </script>`

func TestBuildGoogleTagManager(t *testing.T) {
	Convey("TestBuildGoogleTagManager", t, func() {
		So(BuildGoogleTagManager("123"), ShouldEqual, gtmExpected)
	})
}